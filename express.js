const express = require('express');
const path = require('path');
const port = 4000;

const app = express();

app.use('/static', express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/views/main.html'));
});

app.get('/index.html', (req, res) => {
  res.sendFile(path.join(__dirname, '/views/index.html'));
});

app.get('/user-create.html', (req, res) => {
  res.sendFile(path.join(__dirname, '/views/user-create.html'));
});

app.get('/main.html', (req, res) => {
  res.sendFile(path.join(__dirname, '/views/main.html'));
});

app.get('/user-view.html', (req, res) => {
  res.sendFile(path.join(__dirname, '/views/user-view.html'));
});

app.get('/user-edit.html', (req, res) => {
  res.sendFile(path.join(__dirname, '/views/user-edit.html'));
});

app.get('/user-delete.html', (req, res) => {
  res.sendFile(path.join(__dirname, '/views/user-delete.html'));
});

app.listen(port, () => {
  console.log(`App is listening on port: ${port}`);
});
