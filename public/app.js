let users = [];
let increment = 0;
let dateModified = null;
let indexRow;

let username;
let fullname;
let password;
let confirmPassword;

var LogicFunctions = {
  saveUser: (event) => {
    username = document.getElementById('username').value;
    fullname = document.getElementById('fullname').value;
    password = document.getElementById('password').value;
    confirmPassword = document.getElementById('password-confirm').value;

    var errorUsername = document.getElementById('username-error');
    var errorFullname = document.getElementById('fullname-error');
    var errorPassword = document.getElementById('password-error');

    if (username === '') {
      document.getElementById('username').focus();
      errorUsername.textContent = 'Username is required';
      return false;
    }
    if (username !== null) {
      errorUsername.textContent = '';
    }
    if (fullname === '') {
      document.getElementById('fullname').focus();
      errorFullname.textContent = 'Fullname is required';
      return false;
    }
    if (fullname !== null) {
      errorFullname.textContent = '';
    }
    if (password === '') {
      document.getElementById('password').focus();
      errorPassword.textContent = 'Password is required';
      return false;
    }
    if (password !== null) {
      errorPassword.textContent = '';
    }
    if (password != confirmPassword) {
      errorPassword.innerHTML = 'Pasword does not match.';
      return false;
    } else {
      let user = {
        id: new Date(),
        username,
        fullname,
        password,
        dateModified,
      };
      users.push(user);
      document.forms[0].reset();
      event.preventDefault();
      saveTodoUser();
      alert('Successfuly Saved!');
    }
  },
  loadusers: () => {
    var listAccount = JSON.parse(localStorage.getItem('Users'));
    var tblAccounts = document.getElementById('users-list');
    if (listAccount !== null) {
      var tabledata = listAccount
        .map(
          (accounts) =>
          `
                    <tr onclick="getRowIndex(this)">
                        <td>${accounts.username}</td>
                        <td>${accounts.fullname}</td>
                        <td><a href="user-view.html"><button class="btn" id="btn">View</button></a></td>
                        <td><a href="user-edit.html"><button class="btn" id="btn">Edit</button></a></td>
                        <td><a href="user-delete.html"><button class="btn" id="btn">Delete</button></a></td>
                    </tr>
                `
        )
        .join('');
      var tbody = document.querySelector('#tbody');
      if (tbody != null) {
        tbody.innerHTML = tabledata;
      }
    }
  },
  loaduser: () => {
    const listAccount = JSON.parse(localStorage.getItem('Users'));
    const select = JSON.parse(sessionStorage.getItem('Id'));
    if (select !== null) {
      $('#tbodyuser').append(`<tr>
            <td class="fullname">${listAccount[select].fullname}</td>
            <td class="username">${listAccount[select].username}</td>
            <td class="datecreated">${listAccount[select].id}</td>
            <td class="datemodified">${listAccount[select].dateModified}</td>
            </tr>`);
    }
  },
  loadupdateuser: () => {
    let select;
    const listAccount = JSON.parse(localStorage.getItem('Users'));
    if (sessionStorage.id !== null) {
      select = JSON.parse(sessionStorage.getItem('Id'));
    }
    if (select !== null) {
      $('#tbodyupdateuser').append(`
            <tr>
                <td><p>Fullname:</p></td>
                <td><input type="text" name="fullname" id="fullname" value="${listAccount[select].fullname}" /></td> 
            </tr>
            <tr>
                <td><p>Password:</p></td>
                <td><input type="password" name="password" id="password" /></td>
            </tr>
            </tr>
                <td><p>Confirm Password:</p></td>
                <td><input type="password" name="confirm-password" id="confirm-password" /></td>
            </tr>
            `);
    }
  },
  updateuser: () => {
    let index;
    const listAccount = JSON.parse(localStorage.getItem('Users'));
    if (sessionStorage.id !== null) {
      index = JSON.parse(sessionStorage.getItem('Id'));
    }
    let selectid = listAccount[index].id;
    let selectusername = listAccount[index].username;
    let selectpassword = listAccount[index].password;
    let selectdatemodified = listAccount[index].dateModified;

    fullname = document.getElementById('fullname').value;
    password = document.getElementById('password').value;
    confirm_password = document.getElementById('confirm-password').value;

    if (selectpassword !== password) {
      alert('Incorrect Password \n Please try again');
      document.forms['formupdateuser']['password'].focus();
      return;
    }
    if (selectpassword === password) {
      if (password !== confirm_password) {
        alert('The password confirmation does not match');
        document.forms['formupdateuser']['confirm-password'].focus();
        return;
      } else {
        let user = listAccount;
        user[index].fullname = fullname;
        user[index].dateModified = new Date();
        localStorage.setItem('Users', JSON.stringify(user));
        alert('done update');
      }
    }
  },
  loaddeleteuser: () => {
    var listAccount = JSON.parse(localStorage.getItem('Users'));
    if (sessionStorage.id !== null) {
      var select = JSON.parse(sessionStorage.getItem('Id'));
    }
    if (select !== null) {
      $('#tbodydeleteuser').append(`
            <tr>
                <td><p>Fullname:</p></td>
                <td><input type="text" name="fullname" id="fullname" value="${listAccount[select].fullname}" readonly /></td> 
            </tr>
            <tr>
                <td><p>Username:</p></td>
                <td><input type="text" name="username" id="username" value="${listAccount[select].username}" readonly /></td>
            </tr>
            `);
    }
  },
  deleteuser: (ev) => {
    var listAccount = JSON.parse(localStorage.getItem('Users'));
    if (sessionStorage.id !== null) {
      let index = JSON.parse(sessionStorage.getItem('Id'));
    }
    users.splice(index, 1);
    localStorage.setItem('Users', JSON.stringify(users));
    alert('deleted');
    document.forms['formdeleteuser']['fullname'].value = '';
    document.forms['formdeleteuser']['username'].value = '';
    sessionStorage.removeItem('Id');
  },
};
//display local storage data to index.html
window.onload = () => {
  //loadpages
  LogicFunctions.loadusers();
  if (sessionStorage.Id !== null) {
    LogicFunctions.loaduser();
    LogicFunctions.loadupdateuser();
    LogicFunctions.loaddeleteuser();
  }
  //events: add,edit,delete
  var formAdd = document.getElementById('registration-form');
  if (formAdd !== null) {
    formAdd.addEventListener('submit', LogicFunctions.saveUser, false);
  }
  var btnUpdate = document.getElementById('btnupdate');
  if (btnUpdate !== null) {
    btnUpdate.addEventListener('click', LogicFunctions.updateuser, false);
  }
  var btnDelete = document.getElementById('btndelete');
  if (btnDelete !== null) {
    btnDelete.addEventListener('click', LogicFunctions.deleteuser, false);
  }
};

const getRowIndex = (x) => {
  indexRow = x.rowIndex - 1;
  indexRow = JSON.stringify(indexRow);
  sessionStorage.setItem('Id', indexRow);
  return indexRow;
};

const saveTodoUser = () => {
  var str = JSON.stringify(users);
  localStorage.setItem('Users', str);
};

const getTodoUser = () => {
  var str = localStorage.getItem('Users');
  users = JSON.parse(str);
  if (!users) {
    users = [];
  }
};
getTodoUser();